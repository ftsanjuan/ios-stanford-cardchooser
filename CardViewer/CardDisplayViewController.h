//
//  CardDisplayViewController.h
//  CardViewer
//
//  Created by Francis San Juan on 2013-08-16.
//  Copyright (c) 2013 Francis San Juan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardDisplayViewController : UIViewController

@property (nonatomic) NSUInteger rank;
@property (nonatomic, strong) NSString *suit;

@end
