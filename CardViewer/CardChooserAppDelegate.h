//
//  CardChooserAppDelegate.h
//  CardViewer
//
//  Created by Francis San Juan on 2013-08-16.
//  Copyright (c) 2013 Francis San Juan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardChooserAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
