//
//  CardChooserViewController.m
//  CardViewer
//
//  Created by Francis San Juan on 2013-08-16.
//  Copyright (c) 2013 Francis San Juan. All rights reserved.
//

#import "CardChooserViewController.h"
#import "CardDisplayViewController.h"

@interface CardChooserViewController ()

@property (weak, nonatomic) IBOutlet UISegmentedControl *suitSegmentedControl;
@property (weak, nonatomic) IBOutlet UILabel *rankLabel;
@property (nonatomic) NSUInteger rank;
@property (nonatomic, readonly) NSString *suit;

@end

@implementation CardChooserViewController

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  // note that at this point, the destination view controller's outlets have
  // not yet been loaded...
  if ([segue.identifier isEqualToString:@"ShowCard"]) {
    if ([segue.destinationViewController isKindOfClass:[CardDisplayViewController class]]) {
      CardDisplayViewController *cdvc = (CardDisplayViewController *)segue.destinationViewController;
      cdvc.suit = self.suit;
      cdvc.rank = self.rank;
      cdvc.title = [[self rankAsString] stringByAppendingString:self.suit];
    }
  }
}

- (NSString *)rankAsString
{
  return @[@"?", @"A", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", @"J", @"Q", @"K",][self.rank];
}

- (void)setRank:(NSUInteger)rank
{
  _rank = rank;
  self.rankLabel.text = [self rankAsString];
}


- (NSString *)suit
{
  return [self.suitSegmentedControl titleForSegmentAtIndex:self.suitSegmentedControl.selectedSegmentIndex];
}

- (IBAction)changeRank:(UISlider *)sender
{
  // note that a slider's value is a float
  self.rank = round(sender.value);
}

@end
