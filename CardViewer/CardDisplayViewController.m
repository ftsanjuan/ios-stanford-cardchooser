//
//  CardDisplayViewController.m
//  CardViewer
//
//  Created by Francis San Juan on 2013-08-16.
//  Copyright (c) 2013 Francis San Juan. All rights reserved.
//

#import "CardDisplayViewController.h"
#import "PlayingCardView.h"

@interface CardDisplayViewController ()

@property (weak, nonatomic) IBOutlet PlayingCardView *playingCardView;

@end

@implementation CardDisplayViewController


- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  self.playingCardView.rank = self.rank;
  self.playingCardView.suit  = self.suit;
  self.playingCardView.faceUp = YES;
}

@end